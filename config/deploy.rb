# config valid only for Capistrano 3.1
lock '3.1.0'

application = 'contacts_keeper'
set :application, application
set :repo_url, 'git@bitbucket.org:k4ir05/contacts_keeper.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
#set :pty, true

# Default value for :linked_files is []
#set :linked_files, %w{config/database.yml Procfile config/unicorn.rb}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rvm_type, :user
set :rvm_ruby_version, 'ruby-2.0.0-p451@contacts_keeper'

namespace :foreman do

  desc 'Start server'
  task :start do
    on roles(:all) do
      sudo "start #{application}"
    end
  end

  desc 'Stop server'
  task :stop do
    on roles(:all) do
      sudo "stop #{application}"
    end
  end

  desc 'Restart server'
  task :restart do
    on roles(:all) do
      sudo "restart #{application}"
    end
  end

  desc 'Server status'
  task :status do
    on roles(:all) do
      execute "initctl list | grep #{application}"
    end
  end
end

namespace :deploy do

  desc 'Setup'
  task :setup do
    on roles(:all) do
      deploy_path = fetch :deploy_to
      execute "mkdir -p #{shared_path}/config/"
      upload!('shared/database.yml', "#{shared_path}/config/database.yml")
      upload!('shared/Procfile', "#{shared_path}/Procfile")
      upload!('shared/nginx.conf', "#{shared_path}/nginx.conf")
      upload!('shared/unicorn.rb', "#{shared_path}/config/unicorn.rb")
      upload!('shared/.env.rb', "#{shared_path}/.env")
      sudo 'mkdir -p /usr/local/nginx/conf/sites'
      sudo "ln -sf #{shared_path}/nginx.conf /usr/local/nginx/conf/sites/#{fetch(:application)}.conf"
      #sudo 'nginx -s reload'
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'db:create'
        end
      end
    end
  end

  desc 'Create symlink'
  task :symlink do
    on roles(:all) do
      execute "ln -sf #{shared_path}/config/database.yml #{release_path}/config/database.yml"
      execute "ln -sf #{shared_path}/config/unicorn.rb #{release_path}/config/unicorn.rb"
      execute "ln -sf #{shared_path}/Procfile #{release_path}/Procfile"
      execute "ln -sf #{shared_path}/.env #{release_path}/.env"
    end
  end

  desc 'Foreman init'
  task :foreman_init do
    on roles(:all) do
      foreman_temp = "/var/www/tmp/foreman"
      execute "mkdir -p #{foreman_temp}"
      execute "ln -sf #{release_path} #{current_path}"

      within current_path do
        execute "cd #{current_path}"
        execute :bundle, "exec foreman export upstart #{foreman_temp} -a #{fetch(:application)} -u deployer -l #{shared_path}/log -d #{current_path}"
      end
      sudo "mv #{foreman_temp}/* /etc/init/"
      sudo "rm -r #{foreman_temp}"
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      sudo "restart #{application}"
    end
  end

  after :publishing, :restart

  #after :restart, :clear_cache do
  #  on roles(:web), in: :groups, limit: 3, wait: 10 do
  #    # Here we can do anything such as:
  #    within release_path do
  #      execute :rake, 'tmp:clear'
  #    end
  #  end
  #end

  before :setup, 'set_rails_env'
  before :foreman_init, 'set_rails_env'
  #after :finishing, 'deploy:cleanup'
  #after :finishing, 'deploy:restart'
  after :updating, 'deploy:symlink'
  after :setup, 'deploy:foreman_init'
  after :foreman_init, 'foreman:start'
  before :foreman_init, 'rvm:hook'
  before :setup, 'deploy:check:make_linked_dirs'
  before :setup, 'deploy:starting'
  before :setup, 'deploy:updating'
  before :setup, 'bundler:install'
end
