ContactsKeeper::Application.routes.draw do

  root to: 'home#index'
  devise_for :users, controllers: {registrations: 'registrations'}
  resources :users, only: [:index, :show, :update, :destroy]
  #resources :contacts_backups, except: [:show, :new, :create, :edit] do
  resources :contacts_backups, only: [:index, :destroy] do
    patch :set_actual, on: :member
  end

  mount API => '/'

end
