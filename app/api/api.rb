class API < Grape::API
  prefix 'api'
  version 'v1', using: :accept_version_header
  default_format :json
  format :json

  REALM = 'ContactsKeeper'

  helpers do
    def logger
      API.logger
    end

    def authorize!(*args)
      ::Ability.new(current_user).authorize!(*args)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end

    def get_username(env)
      auth = env['HTTP_AUTHORIZATION']
      ActiveSupport::HashWithIndifferentAccess[auth.to_s.gsub(/^Digest\s+/, '').split(',').map do |pair|
        key, value = pair.split('=', 2)
        [key.strip, value.to_s.gsub(/^"|"$/,'').delete('\'')]
      end]['username']
    end

    def current_user
      @current_user ||= User.where(["lower(name) = :value", { value: get_username(env).mb_chars.downcase.to_s }]).first
    end
  end

  auth :http_digest, {realm: {realm: REALM, passwords_hashed: true, opaque: ContactsKeeper::Application.config.secret_key_base}} do |username|
    user = User.where(["lower(name) = :value", { value: username.mb_chars.downcase.to_s }]).first
    user.present? ? user.api_password : nil
  end

  rescue_from CanCan::AccessDenied do |e|
    Rack::Response.new({error: e.message}, 403, { "Content-type" => "text/error" }).finish
  end

  before do
    authenticate!
  end

  desc 'Check authorization'
  get 'auth' do
    if (contact_backup = current_user.actual_contacts_backup).present?
      {backup: contact_backup.as_json(only: [:name, :phone, :system])}
    else
      {backup: {}}
    end
  end

  desc 'Creates new contacts backup.'
  params do
    requires :name, type: String
    requires :phone, type: String
    requires :data, type: String
    requires :system, type: String
  end
  post 'backup' do
    authorize! :create, ContactsBackup
    contact_backup = ContactsBackup.new user_id: current_user.id, name: params[:name], phone: params[:phone], data: params[:data], system: params[:system]
    if contact_backup.save
      present contact_backup.as_json(only: :id)
    else
      error!({error: contact_backup.errors.full_messages.join('. ')}, 400)
    end
  end

  desc 'Returns actual backup'
  get 'actual_backup' do
    contact_backup = current_user.actual_contacts_backup
    authorize! :read, contact_backup
    {backup: contact_backup.as_json(only: [:name, :phone, :system, :data])}
  end

end
