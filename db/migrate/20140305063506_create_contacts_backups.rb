class CreateContactsBackups < ActiveRecord::Migration
  def change
    create_table :contacts_backups do |t|
      t.references :user, index: true
      t.string :name
      t.string :phone
      t.text :data
      t.boolean :is_actual

      t.timestamps
    end
  end
end
