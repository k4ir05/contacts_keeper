class AddApiPasswordToUsers < ActiveRecord::Migration
  def change
    add_column :users, :api_password, :string, default: '', null: false
  end
end
