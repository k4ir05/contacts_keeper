class ContactsBackupsController < ApplicationController
  before_action :set_contacts_backup, only: [:show, :edit, :update, :destroy, :set_actual]
  authorize_resource

  def index
    @contacts_backups = current_user.is_admin? ? ContactsBackup.all : current_user.contacts_backups.actual
    @contacts_backups = @contacts_backups.search(params).by_created_desc.page params[:page]
    respond_to do |format|
      format.html
      format.js { render 'shared/index' }
    end
  end

  #def edit
  #end

  #def update
  #  respond_to do |format|
  #    if @contacts_backup.update(contacts_backup_params)
  #      format.html { redirect_to contacts_backups_path, notice: 'Contacts backup was successfully updated.' }
  #      format.json { head :no_content }
  #    else
  #      format.html { render action: 'edit' }
  #      format.json { render json: @contacts_backup.errors, status: :unprocessable_entity }
  #    end
  #  end
  #end

  def destroy
    @contacts_backup.destroy
    respond_to do |format|
      format.html { redirect_to contacts_backups_url }
    end
  end

  def set_actual
    @contacts_backup.update_attribute :is_actual, true
    @contacts_backups = @contacts_backup.user.contacts_backups(true)
    respond_to do |format|
      format.js
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_contacts_backup
    @contacts_backup = ContactsBackup.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def contacts_backup_params
    params.require(:contacts_backup).permit(:is_actual, :name, :phone, :data)
  end

end
