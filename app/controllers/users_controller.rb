class UsersController < ApplicationController
  authorize_resource

  def index
    #authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.all.search(params).page params[:page]
    respond_to do |format|
      format.html
      format.js { render 'shared/index' }
    end
  end

  def show
    @user = User.find(params[:id])
  end
  
  def update
    #authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)#, as: :admin)
      redirect_to users_path, notice: "User updated."
    else
      redirect_to users_path, alert: "Unable to update user."
    end
  end

  def destroy
    #authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, notice: "User deleted."
    else
      redirect_to users_path, notice: "Can't delete yourself."
    end
  end

  private

  def user_params
    params.require(:user).permit(:role_ids)
  end

end