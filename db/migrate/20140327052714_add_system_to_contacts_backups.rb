class AddSystemToContactsBackups < ActiveRecord::Migration
  def change
    add_column :contacts_backups, :system, :string
  end
end
