require 'spec_helper'

describe "ContactsBackups" do
  describe "GET /contacts_backups" do
    it "redirects to authorization page" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get contacts_backups_path
      response.status.should be(302)
    end
  end
end

describe 'API' do
  context "without authorization" do
    it "returns auth params" do
      post "/api/backup", parameters: attributes_for(:contacts_backup).to_json, 'Content-Type' => 'application/json', 'Accept-Version' => 'v1'
      response.status.should be 401
      response.body.should eq ''
    end
  end

  context 'with autorization' do
    subject(:user) { create :user, password: '12345678', password_confirmation: '12345678' }

    def auth_header_value(email, password, method, path)
      uri = URI.parse 'http://localhost:4000'
      headers = {'Accept' => 'application/json', 'Content-Type' => 'application/json', 'Accept-Version' => 'v1'}
      if method == 'POST'
        request = Net::HTTP::Post.new path, headers
      else
        request = Net::HTTP::Get.new path, headers
      end
      http = Net::HTTP.new uri.host, uri.port
      auth_response = http.request(request)['WWW-Authenticate']
      auth_keys = ActiveSupport::HashWithIndifferentAccess[auth_response.to_s.gsub(/^Digest\s+/, '').split(',').map do |pair|
        key, value = pair.split('=', 2)
        [key.strip, value.to_s.gsub(/^"|"$/,'').delete('\'')]
      end]
      realm = auth_keys['realm']
      nonce = auth_keys['nonce']
      opaque = auth_keys['opaque']
      qop = auth_keys['qop']

      ha1 = Digest::MD5.hexdigest [email, realm, password]*':'
      ha2 = Digest::MD5.hexdigest [method, path]*':'
      digest_response = Digest::MD5.hexdigest [ha1, nonce, ha2]*':'
      "Digest username=#{email}, realm=#{realm}, nonce=#{nonce}, uri=#{path}, qop=#{qop}, opaque=#{opaque}, response=#{digest_response}"
    end

    def authorized_request(email, password, method, path, data={})
      uri = URI.parse 'http://0.0.0.0:4000'
      headers = {'Accept' => 'application/json', 'Content-Type' => 'application/json', 'Accept-Version' => 'v1'}
      if method == 'POST'
        request = Net::HTTP::Post.new path, headers
      else
        request = Net::HTTP::Get.new path, headers
      end
      http = Net::HTTP.new uri.host, uri.port
      auth_response = http.request(request)['WWW-Authenticate']
      auth_keys = ActiveSupport::HashWithIndifferentAccess[auth_response.to_s.gsub(/^Digest\s+/, '').split(',').map do |pair|
        key, value = pair.split('=', 2)
        [key.strip, value.to_s.gsub(/^"|"$/,'').delete('\'')]
      end]
      realm = auth_keys['realm']
      nonce = auth_keys['nonce']
      opaque = auth_keys['opaque']
      qop = auth_keys['qop']

      ha1 = Digest::MD5.hexdigest [email, realm, password]*':'
      ha2 = Digest::MD5.hexdigest [method, path]*':'
      digest_response = Digest::MD5.hexdigest [ha1, nonce, ha2]*':'
      headers.merge!('Authorization' => "Digest username=\"#{email}\", realm=\"#{realm}\", nonce=\"#{nonce}\", uri=\"#{path}\", qop=#{qop}, opaque=\"#{opaque}\", response=\"#{digest_response}\"")
      if method == 'POST'
        post path, headers.merge(data)
        #request = Net::HTTP::Post.new path, headers.merge(data)
      else
        get path, headers
        #request = Net::HTTP::Get.new path, headers
      end
      #http.request(request)
    end

    describe "POST /backup" do
      it "creates new backup and returns it's id" do
        #authorized_request user.email, '12345678', 'POST', '/api/backup', {'name' => 'name', 'phone' => 'phone', 'data' => 'data'}
        auth_value = auth_header_value(user.email, '12345678', 'POST', '/api/backup')
        post '/api/backup', 'Content-Type' => 'application/json', 'Accept-Version' => 'v1', 'Authorization' => auth_value, 'name' => 'name', 'phone' => 'phone', 'data' => 'data'
        response.status.should be(200)
      end
    end

    describe "GET /actual_backup" do
      it "returns actual backup" do
        #authorized_request user.email, '12345678', 'GET', '/api/actual_backup'
        auth_value = auth_header_value(user.email, '12345678', 'GET', '/api/actual_backup')
        get '/api/actual_backup', 'Content-Type' => 'application/json', 'Accept-Version' => 'v1', 'Authorization' => auth_value
        response.status.should be(200)
      end
    end
  end
end
