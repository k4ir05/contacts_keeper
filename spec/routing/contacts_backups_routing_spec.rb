require "spec_helper"

describe ContactsBackupsController do
  describe "routing" do

    it "routes to #index" do
      get("/contacts_backups").should route_to("contacts_backups#index")
    end

    it "routes to #new" do
      get("/contacts_backups/new").should route_to("contacts_backups#new")
    end

    it "routes to #show" do
      get("/contacts_backups/1").should route_to("contacts_backups#show", :id => "1")
    end

    it "routes to #edit" do
      get("/contacts_backups/1/edit").should route_to("contacts_backups#edit", :id => "1")
    end

    it "routes to #create" do
      post("/contacts_backups").should route_to("contacts_backups#create")
    end

    it "routes to #update" do
      put("/contacts_backups/1").should route_to("contacts_backups#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/contacts_backups/1").should route_to("contacts_backups#destroy", :id => "1")
    end

  end
end
