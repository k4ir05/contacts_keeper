# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contacts_backup do
    user
    name "Name"
    phone "3829485"
    data "data"*50000
    is_actual false
    system 'iOS'
  end
end