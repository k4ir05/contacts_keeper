# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ContactsKeeper::Application.config.secret_key_base = 'f2ee003c376c6ffa2678e1fea78c08bb69e2c64767117610a4faa79658f8fe26b3431676af99012da5ec74db52ce0440d260824877be9beb49846e8a69c63036'
