jQuery ->
  $("a[rel~=popover], a[data-toggle=popover], .has-popover").popover()
  $("a[rel~=tooltip], a[data-toggle=tooltip], .has-tooltip").tooltip()
  $(".collapse").collapse()
  $(".dropdown-toggle").dropdown()

$(document).on 'click', '#search_form .clear_search_input', (event) ->
  $(this).parent().siblings('.form-control').val ''
  $('#search_form').submit()

window.removeTooltips = ->
  $('.tooltip').remove()