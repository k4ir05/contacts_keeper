# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Environment variables (ENV['...']) can be set in the file .env file.
puts 'ROLES'
YAML.load(ENV['ROLES']).each do |role|
  Role.where(name: role).first_or_create!
  puts 'role: ' << role
end
puts 'DEFAULT USERS'
user = User.where(email: ENV['ADMIN_EMAIL'].dup).first_or_create!(name: ENV['ADMIN_NAME'].dup, password: ENV['ADMIN_PASSWORD'].dup, password_confirmation: ENV['ADMIN_PASSWORD'].dup)
# user.confirm!
puts 'user: ' << user.name
user.add_role :admin