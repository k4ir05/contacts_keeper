class ContactsBackup < ActiveRecord::Base
  scope :by_created_desc, -> { order('contacts_backups.created_at desc') }
  scope :actual, -> { where(is_actual: true) }
  belongs_to :user
  delegate :name, to: :user, prefix: true, allow_nil: true
  validates_presence_of :name, :phone, :data, :user, :system
  validates_uniqueness_of :is_actual, scope: :user_id, if: Proc.new { |b| b.is_actual }
  before_save :clear_actual, :set_actual

  def self.search(params)
    contacts_backups = ContactsBackup.all

    if (q = params[:q]).present?
      contacts_backups = contacts_backups.where ["lower(contacts_backups.name) LIKE :value OR lower(contacts_backups.phone) LIKE :value", { value: "%#{q.mb_chars.downcase.to_s}%" }]
    end

    if (user_q = params[:user_q]).present?
      contacts_backups = contacts_backups.includes(:user).where ["lower(users.name) LIKE :value OR lower(users.email) LIKE :value", { value: "%#{user_q.mb_chars.downcase.to_s}%" }]
    end

    contacts_backups
  end

  private

  def clear_actual
    self.user.contacts_backups(true).actual.each { |b| b.update_column(:is_actual, false) }
  end

  def set_actual
    self.is_actual = true
  end

end
