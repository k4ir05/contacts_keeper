class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :registerable#, :confirmable
  cattr_accessor :current
  attr_accessor :login
  validates :name, uniqueness: {case_sensitive: false}, presence: true, length: {minimum: 4}
  before_save :generate_api_password

  has_many :contacts_backups, dependent: :destroy

  def actual_contacts_backup
    contacts_backups.actual.first
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(name) = :value OR lower(email) = :value", { value: login.mb_chars.downcase.to_s }]).first
    else
      where(conditions).first
    end
  end

  def self.search(params)
    users = User.all
    if (q = params[:q]).present?
      users = users.where ["lower(name) LIKE :value OR lower(email) LIKE :value", { value: "%#{q.mb_chars.downcase.to_s}%" }]
    end
    users
  end

  private

  def generate_api_password
    self.api_password = Digest::MD5.hexdigest [self.name, API::REALM, self.password]*':' unless self.password.blank?
  end

end
