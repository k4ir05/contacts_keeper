require 'spec_helper'

describe ContactsBackup do

  it 'is valid with valid attributes' do
    expect(build(:contacts_backup)).to be_valid
  end

  context 'actual' do
    subject(:user) { create :user }

    it 'sets new backup actual' do
      contacts_backup = ContactsBackup.create attributes_for(:contacts_backup).merge(user_id: user.id)
      expect(contacts_backup.is_actual).to be_true
    end

    context 'validation' do
      before :each do
        @contacts_backup1 = create :contacts_backup
        @contacts_backup2 = create :contacts_backup, user_id: user.id
      end

      it 'should have only one actual contacts backup per user' do
        contacts_backup = ContactsBackup.new attributes_for(:contacts_backup).merge(user_id: user.id, is_actual: true)
        expect(contacts_backup).to_not be_valid
      end
    end

    context 'callbacks' do
      it 'changes actual to new backup' do
        ContactsBackup.create attributes_for(:contacts_backup).merge(user_id: user.id)
        new_contacts_backup = ContactsBackup.create attributes_for(:contacts_backup).merge(user_id: user.id)
        expect(user.actual_contacts_backup.id).to eq new_contacts_backup.id
      end
    end

  end

end
